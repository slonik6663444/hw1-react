import React, {Component} from 'react';
import ProductCard from "../ProductCard/ProductCard";

class Main extends Component {

    state = {
        productsCard:[]
    }

     componentDidMount() {
        fetch("./product.json")
            .then(response => response.json())
            .then(result => this.setState({productsCard: result}))
    }

    render() {
    const {productsCard} = this.state;
        console.log({productsCard});
    const productItem = productsCard.map((item, index) =>
        <ProductCard
            name={item.name}
            price={item.price}
            pathToPic={item.pathToPic}
            article={item.article}
            color={item.color}
            key={index}>
        </ProductCard>)





        return (
            <div className="container">
                <div className="product-card__block">
                    {productItem}
                </div>
            </div>
        );
    }
}

export default Main;